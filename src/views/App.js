import React, { Component } from 'react';
// import logo from './logo.svg';
import '../assets/App.css';
import Home from './HomeComponent';
import Hello from './HelloComponent';
import About from './AboutComponent';
import Books from './BooksComponent';
// import route Components here
import {
  BrowserRouter as Router,
  Route,
  Link
  // Switch,
  // Redirect
} from 'react-router-dom'

class App extends Component {
  render() {
    return (
      
      <Router>
       
        <div className="App">
          <div className="container">
                  <ul>
                  <li><Link to="/hello">Hello</Link></li>
                  <li><Link to="/about">About</Link></li>
                  <li><Link to="/books">Books</Link></li>
                  </ul>
                  <hr/>
                  
        {/* From Internal File */}          
        {/* If from home redirect to another url 
        <Route exact={true} path="/" component={Home} /> */}

        <Route path="/" component={Home} />
        <Route path="/hello" component={Hello} />
        <Route path="/about" component={About} />
        <Route path="/books" component={Books} />

        {/* Not From External file */
        /* 
        <Route path="/hello" render={() => {
          return (
             <div className="jumbotron">
               <h1 className="display-3">Hello, say hello!</h1>
             </div>
            );
        }}/>
        <Route path="/about" render={() => {
          return (
             <div className="jumbotron">
               <h1 className="display-3">Hello, About!</h1>
             </div>
            );
        }}/>
        <Route path="/books" render={() => {
          return (
             <div className="jumbotron">
               <h1 className="display-3">Hello, books!</h1>
             </div>
            );
        }}/> */}
        
          </div>
      </div>
      </Router>
      
    );
  }
}

export default App;
